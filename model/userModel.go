package model

import (
	"time"
	
	"language-helper-server/database"
	"language-helper-server/service"
	"language-helper-server/structure"
	"github.com/go-pg/pg"
)

func createToken(user *structure.User) error {
	expiredAt := service.GetToken(user)
	token := structure.Token{
		Token:     user.Token,
		UserId:    user.Id,
		ExpiredAt: time.Unix(expiredAt, 0)}
	
	dbManager := database.GetDBManager()
	_, err := dbManager.Db.Model(&token).
		OnConflict("(user_id) DO UPDATE").
		Set("token = ?token").
		Set("expired_at = ?expired_at").
		Insert()
	
	return err
}

func UserByEmailExists(email string) (bool, error) {
	dbManager := database.GetDBManager()
	userExist := &structure.User{}
	count, err := dbManager.Db.Model(userExist).Where("email = ?", email).Count()
	return count != 0, err
}

func CreateUserWithWithToken(user *structure.User) error {
	
	dbManager := database.GetDBManager()
	return dbManager.Db.RunInTransaction(func(tx *pg.Tx) error {
		
		err := dbManager.Db.Insert(user)
		if err != nil {
			tx.Rollback()
			return err
		}
		
		err = createToken(user)
		if err != nil {
			tx.Rollback()
			return err
		}
		
		//tx.Commit()
		return nil
	})
}

func FindUserByEmail(email string, isTokenNeed bool) (user *structure.User, err error) {
	dbManager := database.GetDBManager()
	userExist := &structure.User{}
	err = dbManager.Db.Model(userExist).Where("email = ?", email).Select()
	if err == nil && isTokenNeed {
		err = createToken(userExist)
	}
	return userExist, err
}

func SaveUserSearch(userId int, searchText string) error {
	searchRequest := structure.UserSearch{SearchText: searchText, UserId: userId}
	dbManager := database.GetDBManager()
	_, err := dbManager.Db.Model(&searchRequest).
		OnConflict(`(user_id, search_text) DO UPDATE SET "count" = (user_search."count" + 1)`).
		Set("token = ?token").
		Set("expired_at = ?expired_at").
		Insert()
	return err
}

func FindUserSearchesById(userId int) (*[]structure.UserSearch, error) {
	dbManager := database.GetDBManager()
	userSearches := &[]structure.UserSearch{}
	err := dbManager.Db.Model(userSearches).Where("user_id = ?", userId).Order("updated_at DESC").Select()
	if err != nil && err != pg.ErrNoRows {
		return nil, err
	}
	return userSearches, nil
}

func CheckTokenExists(token string) (bool, error) {
	dbManager := database.GetDBManager()
	tokenExist := &structure.Token{}
	count, err := dbManager.Db.Model(tokenExist).Where("token = ?", token).Count()
	return count != 0, err
}
