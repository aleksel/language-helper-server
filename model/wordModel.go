package model

import (
	"language-helper-server/database"
	"language-helper-server/structure"
)

func GetWords(wordString string, wordDb *[]structure.Word) error {
	dbManager := database.GetDBManager()
	_, err := dbManager.Db.Model(wordDb).
		Query(wordDb, `SELECT * FROM (
			SELECT
				DISTINCT ON (ws.id, ps.id)
				ws.id,
				ws.word,
				ws.infinitive,
				language.similarity(word, '`+ wordString+ `') AS similarity,
				ps.id AS part_of_speech_id,
				ps.name AS part_of_speech_name,
				length(word) AS length,
				ts_headline(word, q) AS highlight
			FROM
				language.words AS ws
				LEFT JOIN language.word_has_translate AS wht ON wht.word_id = ws."id"
				LEFT JOIN language.parts_of_speech AS ps ON ps."id" = wht.part_of_speech_id,
				plainto_tsquery('`+ wordString+ `') AS q
			WHERE
				language.make_tsvector (word) @@ q
			ORDER BY
				ws.id, ps.id
			LIMIT 500
		) main
		ORDER BY
            similarity DESC;`)
	return err
}

func GetWordWithTranslate(wordId int, wordDb *[]structure.WordWithTranslate) error {
	dbManager := database.GetDBManager()
	err := dbManager.Db.Model(wordDb).
		Column("word.id", "word", "infinitive", "parts_of_speech.name", "translates.text").
		Join("LEFT JOIN language.word_has_translate ON word.id = word_has_translate.word_id").
		Join("LEFT JOIN language.parts_of_speech ON parts_of_speech.id = word_has_translate.part_of_speech_id").
		Join("LEFT JOIN language.translates ON translates.id = word_has_translate.translate_id").
		Where("word.id = ?", wordId).
	//Where("parts_of_speech.id = ?", partOfSpeechId).
		Select()
	
	return err
}

func FindWords(searchString string) (*[]structure.Word, error) {
	dbManager := database.GetDBManager()
	words := &[]structure.Word{}
	_, err := dbManager.Db.Model(words).
		Query(words, `SELECT
			ws.word,
			ws.infinitive,
			language.similarity ( word, '`+ searchString+ `' ) AS similarity,
			LENGTH ( word ) AS length,
			ts_headline( word, q ) AS highlight
		FROM
			language.words AS ws,
			plainto_tsquery( '`+ searchString+ `' ) AS q
		WHERE
			language.make_tsvector ( word ) @@q OR word LIKE '` + searchString + `%'
		ORDER BY
			similarity DESC, length
		LIMIT 50;`)
	return words, err
}
