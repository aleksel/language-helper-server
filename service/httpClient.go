package service

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

var netClient = &http.Client{
	Timeout: time.Second * 10,
}

func Get(url string, res interface{}) error {
	response, err := netClient.Get(url)
	if err != nil {
		return err
	}
	
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}
	json.Unmarshal(body, &res)
	return nil
}
