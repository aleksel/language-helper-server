package service

import (
	"net/http"
	"time"
	
	"language-helper-server/structure"
	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
)

// Глобальный секретный ключ
var mySigningKey = []byte(`X6YND0KxEB@q75YT%.nm>c86(HG>YV5^
							_90h;lWK0yt[A$Wl-pFZ<z8&wn^$9QtE
							Q_eAcv;mQxydKRW<0XQ%RP%B+Vf!Q!^z
							wybaXb3qGLUB,A#q%61!gkoH8zPgt$om`)

func GetToken(user *structure.User) int64 {
	// Создаем новый токен
	token := jwt.New(jwt.SigningMethodHS256)
	claims := make(jwt.MapClaims)
	
	// Устанавливаем набор параметров для токена
	claims["firstName"] = user.FirstName
	claims["lastName"] = user.LastName
	claims["email"] = user.Email
	claims["phone"] = user.Phone
	claims["id"] = user.Id
	expiredAt := time.Now().Add(time.Hour * 24 * 365).Unix()
	claims["exp"] = expiredAt
	
	token.Claims = claims
	
	// Подписываем токен нашим секретным ключем
	tokenString, _ := token.SignedString(mySigningKey)
	
	// Отдаем токен клиенту
	user.Token = tokenString
	
	return expiredAt
}

var JwtMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
	ErrorHandler: func(w http.ResponseWriter, r *http.Request, err string) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
	},
	ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
		return mySigningKey, nil
	},
	SigningMethod: jwt.SigningMethodHS256,
})
