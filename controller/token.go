package controller

import (
	"net/http"
	
	"language-helper-server/model"
	"language-helper-server/service"
	"github.com/auth0/go-jwt-middleware"
)

func Jwt(h http.Handler) http.Handler {
	return service.JwtMiddleware.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json;charset=utf-8")
		token, _ := jwtmiddleware.FromAuthHeader(r)
		tokenExists, err := model.CheckTokenExists(token)
		if !tokenExists || err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		h.ServeHTTP(w, r)
	}))
}
