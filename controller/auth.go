package controller

import (
	"encoding/json"
	"net/http"
	
	"language-helper-server/model"
	"language-helper-server/service"
	st "language-helper-server/structure"
)

var UserRegisterHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	
	w.Header().Set("Content-Type", "application/json")
	
	user, err := getUser(w, r)
	if err != nil {
		return
	}
	
	hash, err := service.HashPassword(user.Password)
	if err != nil {
		st.WriteValidationError(map[string]string{"password": "Something wrong with the password"}, w)
		return
	}
	user.Password = hash
	
	exists, err := model.UserByEmailExists(user.Email)
	if err != nil {
		st.WriteServiceError(err, w)
		return
	}
	if exists {
		st.WriteValidationError(map[string]string{"email": "User with the same email already exists"}, w)
		return
	}
	
	err = model.CreateUserWithWithToken(&user)
	if err != nil {
		st.WriteServiceError(err, w)
		return
	}
	
	sendUserInfo(user, w)
})

var UserLoginHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	
	w.Header().Set("Content-Type", "application/json")
	
	user, err := getUser(w, r)
	if err != nil {
		return
	}
	
	userExist, err := model.FindUserByEmail(user.Email, true)
	if err != nil || !service.CheckPasswordHash(user.Password, userExist.Password) {
		st.WriteValidationError(map[string]string{"email": "E-mail or password is incorrect"}, w)
		return
	}
	
	payload, _ := json.Marshal(map[string]string{"token": userExist.Token})
	w.Write([]byte(payload))
})
