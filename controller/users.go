package controller

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	
	"language-helper-server/model"
	st "language-helper-server/structure"
	"github.com/asaskevich/govalidator"
	"github.com/dgrijalva/jwt-go"
	"language-helper-server/utils"
	"language-helper-server/logger"
)

func getUser(w http.ResponseWriter, r *http.Request) (st.User, error) {
	var user st.User
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &user)
	
	_, err := govalidator.ValidateStruct(user)
	if err != nil {
		validationErrors := govalidator.ErrorsByField(err)
		st.WriteValidationError(validationErrors, w)
		return st.User{}, errors.New(utils.ValidationError)
	}
	return user, nil
}

func sendUserInfo(user st.User, w http.ResponseWriter) {
	user.Password = ""
	payload, _ := json.Marshal(user)
	w.Write([]byte(payload))
}

var GetUserProfileHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	
	user := r.Context().Value("user")
	claims := user.(*jwt.Token).Claims.(jwt.MapClaims)
	
	if str, ok := claims["email"].(string); ok {
		userExist, err := model.FindUserByEmail(str, false)
		if err != nil {
			st.WriteSimpleError(http.StatusNotFound, "User not found", w)
			return
		}
		sendUserInfo(*userExist, w)
	} else {
		logger.Warnf("JWT token doesn't contain email field, %v", claims)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("[]"))
	}
})

var GetUserSearchesHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user")
	claims := user.(*jwt.Token).Claims.(jwt.MapClaims)
	
	if userId, ok := claims["id"].(float64); ok {
		userSearches, err := model.FindUserSearchesById(int(userId))
		if err != nil {
			st.WriteServiceError(err, w)
			return
		}
		payload, _ := json.Marshal(userSearches)
		w.Write([]byte(payload))
	} else {
		logger.Warnf("JWT token doesn't contain id field, %v", claims)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("[]"))
	}
})
