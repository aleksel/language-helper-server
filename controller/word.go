package controller

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"regexp"
	"sort"
	"sync"
	
	"language-helper-server/helper"
	"language-helper-server/model"
	"language-helper-server/service"
	st "language-helper-server/structure"
	"language-helper-server/logger"
	"github.com/dgrijalva/jwt-go"
)

const skyengSearchUrl = "http://dictionary.skyeng.ru/api/public/v1/words/search?search="
const skyengMeaningUrl = "http://dictionary.skyeng.ru/api/public/v1/meanings?ids="

var reg, _ = regexp.Compile(`[^a-zA-Z0-9-.,_& ]+`)

var partsOfSpeech = map[string]int{
	"n":      1,
	"prn":    2,
	"det":    3,
	"v":      4,
	"j":      5,
	"r":      6,
	"crd":    7,
	"ord":    7,
	"prp":    8,
	"cjc":    9,
	"exc":    10,
	"md":     12,
	"phl":    27,
	"phv":    28,
	"phf":    29,
	"x":      22,
	"ph0":    23,
	"ph":     23,
	"phi":    23,
	"ph1":    23,
	"abb":    26,
	"prefix": 24,
	"suffix": 25,
}

var GetListWordsDB = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var request st.Request
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &request)
	
	err := helper.ValidateAndWriteResult(request, w)
	if err != nil {
		return
	}
	response := make([]st.Word, 0)
	
	err = model.GetWords(request.Word, &response)
	if err != nil {
		logger.Warn("Error receiving words from db", err)
		st.WriteServiceError(err, w)
		return
	}
	payload, _ := json.Marshal(response)
	w.Write([]byte(payload))
})

var GetDbWord = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var request st.MeaningDbRequest
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &request)
	
	err := helper.ValidateAndWriteResult(request, w)
	if err != nil {
		return
	}
	response := make([]st.WordWithTranslate, 0)
	
	err = model.GetWordWithTranslate(request.Id, &response)
	if err != nil {
		logger.Warn("Error receiving word with translations from db", err)
		st.WriteServiceError(err, w)
		return
	}
	if len(response) == 0 {
		w.WriteHeader(http.StatusNotFound)
	}
	payload, _ := json.Marshal(response)
	w.Write([]byte(payload))
})

type wordDBResponses map[int]st.Word

var GetWordInformation = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var request st.Request
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &request)
	
	err := helper.ValidateAndWriteResult(request, w)
	if err != nil {
		return
	}
	var meanings st.WordResponse
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		err := service.Get(skyengSearchUrl+request.Word, &meanings.SkyResponse)
		if err != nil {
			logger.Warn("Error receiving information from GetWordInformation", err)
		}
	}()
	
	var responseDb []st.Word
	go func() {
		defer wg.Done()
		err2 := model.GetWords(request.Word, &responseDb)
		if err2 != nil {
			st.WriteServiceError(err2, w)
			return
		}
		responseDbMap := make(wordDBResponses, len(responseDb))
		for _, v := range responseDb {
			part := st.PartsOfSpeech{Id: v.PartOfSpeechId, Name: v.PartOfSpeechName}
			if _, ok := responseDbMap[v.Id]; ok {
				temp := responseDbMap[v.Id]
				temp.PartsOfSpeech = append(responseDbMap[v.Id].PartsOfSpeech, part)
				responseDbMap[v.Id] = temp
			} else {
				v.PartsOfSpeech = append(responseDbMap[v.Id].PartsOfSpeech, part)
				responseDbMap[v.Id] = v
			}
		}
		for _, value := range responseDbMap {
			meanings.DbResponse = append(meanings.DbResponse, value)
		}
		sort.Slice(meanings.DbResponse,
			func(i, j int) bool { return meanings.DbResponse[i].Similarity > meanings.DbResponse[j].Similarity })
	}()
	wg.Wait()
	
	user := r.Context().Value("user")
	claims := user.(*jwt.Token).Claims.(jwt.MapClaims)
	var searches *[]st.UserSearch
	
	if userId, ok := claims["id"].(float64); !ok {
		logger.Warnf("JWT token doesn't contain id field, %v", claims)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("[]"))
		return
	} else {
		err = model.SaveUserSearch(int(userId), request.Word)
		if err != nil {
			st.WriteServiceErrorWithDesc("Error save user search request in db", err, w)
			return
		}
		searches, err = model.FindUserSearchesById(int(userId))
		if err != nil {
			st.WriteServiceErrorWithDesc("Error get user searches from db", err, w)
			return
		}
	}
	
	response := st.WordsWithSearchesResponse{Searches: *searches, Words: meanings}
	payload, _ := json.Marshal(response)
	w.Write([]byte(payload))
})

var GetSkyEngWord = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var request st.MeaningRequest
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &request)
	
	err := helper.ValidateAndWriteResult(request, w)
	if err != nil {
		return
	}
	var response []st.SkyEngMeaningResponse
	
	err = service.Get(skyengMeaningUrl+helper.ArrayIntToString(request.Ids), &response)
	if err != nil {
		logger.Warn("Error receiving meanings information from GetWordInformation", err)
		st.WriteServiceError(err, w)
		return
	}
	payload, _ := json.Marshal(response)
	w.Write([]byte(payload))
})

var GetWordSuggestions = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var request st.SuggestionRequest
	body, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(body, &request)
	
	err := helper.ValidateAndWriteResult(request, w)
	if err != nil {
		return
	}
	if len(request.SearchText) < 2 {
		st.WriteValidationError(map[string]string{"word": "There must be at least 2 symbols"}, w)
		return
	}
	
	words, err := model.FindWords(request.SearchText)
	if err != nil {
		st.WriteServiceErrorWithDesc("Error search words in db", err, w)
		return
	}
	payload, _ := json.Marshal(words)
	w.Write([]byte(payload))
})
