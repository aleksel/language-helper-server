-- Query word with translate from particular dictionary and partOfSpeech
SELECT *
FROM
  translates AS tr
  LEFT JOIN words AS ws ON ws."id" = tr.word_id
  LEFT JOIN word_has_part_of_speech AS whps ON ws."id" = whps.word_id
  LEFT JOIN parts_of_speech AS ps ON ps."id" = whps.part_of_speech_id
WHERE
  dictionary_id = 2
  AND ps."id" = 7
LIMIT 20;

-- Query find translates by word or a part of a word
SELECT
  *
FROM
  words ws
  LEFT JOIN word_has_part_of_speech whps ON ws."id" = whps.word_id
  LEFT JOIN parts_of_speech ps ON ps."id" = whps.part_of_speech_id
  LEFT JOIN word_has_translate wht ON ws."id" = wht.word_id
  LEFT JOIN translates t ON t."id" = wht.translate_id
WHERE
  ws.word ILIKE 'rent out%'


-- Query full text search by a word
SELECT
  *, length(word) AS len,
  similarity(word, 'went') AS sim,
  ts_headline(word, q)
FROM
  words,
  to_tsquery('went') AS q
WHERE
make_tsvector (word) @@ q
ORDER BY
len
-- ORDER BY ts_rank(make_tsvector(word), q) DESC

SELECT
  "word"."id",
  "word",
  "infinitive",
  "parts_of_speech"."name",
  "translates"."text"
FROM
  "language".words AS "word"
  LEFT JOIN "language".word_has_translate ON word."id" = word_has_translate.word_id
  LEFT JOIN "language".parts_of_speech ON parts_of_speech."id" = word_has_translate.part_of_speech_id
  LEFT JOIN "language".translates ON translates."id" = word_has_translate.translate_id
WHERE
  (word. ID = 5066)
