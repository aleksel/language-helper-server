package main

import (
	"net/http"
	"os"
	
	hd "language-helper-server/controller"
	_ "language-helper-server/config"
	_ "language-helper-server/logger"
	_ "language-helper-server/database"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	
	// Default page, simple html.
	r.Handle("/", http.FileServer(http.Dir("./views/")))
	
	// Static resources return by certain urls start with /static/{file}
	r.PathPrefix("/static/").
		Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))
	
	r.Handle("/auth/register", hd.UserRegisterHandler).Methods("POST")
	r.Handle("/auth/login", hd.UserLoginHandler).Methods("POST")
	
	r.Handle("/user/get_profile", hd.Jwt(hd.GetUserProfileHandler)).Methods("GET")
	r.Handle("/user/get_user_searches", hd.Jwt(hd.GetUserSearchesHandler)).Methods("GET")
	
	r.Handle("/word/get_word_information", hd.Jwt(hd.GetWordInformation)).Methods("POST")
	r.Handle("/word/get_list_words_db", hd.Jwt(hd.GetListWordsDB)).Methods("POST")
	r.Handle("/word/get_db_word", hd.Jwt(hd.GetDbWord)).Methods("POST")
	r.Handle("/word/get_skyeng_word", hd.Jwt(hd.GetSkyEngWord)).Methods("POST")
	r.Handle("/word/get_word_suggestions", hd.Jwt(hd.GetWordSuggestions)).Methods("POST")
	
	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
	})
	
	//r.Handle("/products/{slug}/feedback", jwtMiddleware.Handler(AddFeedbackHandler)).Methods("POST")
	
	http.ListenAndServe(":3000", handlers.LoggingHandler(os.Stdout, r))
}
