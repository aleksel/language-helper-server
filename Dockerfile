# Version: 0.0.1
FROM alpine:latest

RUN mkdir app

RUN apk add --no-cache tzdata
RUN cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime
RUN echo "Europe/Moscow" >  /etc/timezone

ADD language-helper-server /app/language-helper-server
ADD config/config.yml /app/config.yml

ENTRYPOINT ["/app/language-helper-server", "-c"]

EXPOSE 3000
