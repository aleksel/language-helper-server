package structure

type (
	Config struct {
		Db DBConfiguration `yaml:"db"`
	}
)

type DBConfiguration struct {
	Address      string `valid:"required~Required"`
	Schema       string `valid:"required~Required"`
	Database     string `valid:"required~Required"`
	Port         string `valid:"required~Required"`
	Username     string
	Password     string
	CreateSchema bool    `mapstructure:"create_schema"`
}
