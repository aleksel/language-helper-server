package structure

type WordResponse struct {
	SkyResponse            []SkyEngResponse `json:"skyResponse"`
	DbResponse             []Word           `json:"dbResponse"`
	ReversoContextResponse []string         `json:"reversoContextResponse"`
}

type WordResponses map[string]*WordResponse

type Word struct {
	TableName        string          `sql:"language.words" json:"-"`
	Id               int             `json:"-"`
	Word             string          `json:"word"`
	Infinitive       string          `json:"infinitive"`
	PartOfSpeechId   int             `json:"-"`
	PartOfSpeechName string          `json:"-"`
	Length           int             `json:"-"`
	Similarity       float32         `json:"-"`
	Highlight        string          `json:"highlight"`
	PartsOfSpeech    []PartsOfSpeech `json:"partOfSpeech" pg:",many2many:language.word_has_part_of_speech,joinFK:PartOfSpeech"`
}
type WordWithTranslate struct {
	TableName     string `sql:"language.words" json:"-"`
	Word                 `pg:",override"`
	PartsOfSpeech string `json:"partOfSpeech" sql:"name"`
	Translate     string `json:"translate" sql:"text"`
	//Translates    []Translates `json:"translates" pg:",many2many:language.word_has_translate,joinFK:Translate"`
}

type PartsOfSpeech struct {
	TableName string `sql:"language.parts_of_speech" json:"-"`
	Id        int    `json:"id"`
	Name      string `json:"name"`
}

type Translates struct {
	TableName string `sql:"language.translates" json:"-"`
	Id        int    `json:"id"`
	Text      string `json:"text"`
}

type MeaningDbRequest struct {
	Id int `json:"id" valid:"required~Required"`
	//PartOfSpeechId int `json:"partOfSpeechId" valid:"required~Required"`
}

type WordsWithSearchesResponse struct {
	Words    WordResponse `json:"words"`
	Searches []UserSearch   `json:"searches"`
}
