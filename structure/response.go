package structure

import (
	"encoding/json"
	"net/http"
	
	"language-helper-server/utils"
	"language-helper-server/logger"
)

var responseEmptyMap = make(map[string]string)

type Response struct {
	ErrorMessage string      `json:"errorMessage"`
	Details      interface{} `json:"details"`
}

type ResponseMap struct {
	ErrorMessage string            `json:"errorMessage"`
	Details      map[string]string `json:"details"`
}

func Error(message string, messageDetails interface{}) Response {
	return Response{message, messageDetails}
}

func WriteError(code int, message string, messageDetails interface{}, w http.ResponseWriter) {
	payload, _ := json.Marshal(Error(message, messageDetails))
	w.WriteHeader(code)
	w.Write([]byte(payload))
}

func WriteValidationError(messageDetails interface{}, w http.ResponseWriter) {
	payload, _ := json.Marshal(Error(utils.ValidationError, messageDetails))
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(payload))
}

func WriteSimpleError(code int, message string, w http.ResponseWriter) {
	payload, _ := json.Marshal(Error(message, responseEmptyMap))
	w.WriteHeader(code)
	w.Write([]byte(payload))
}

func WriteServiceError(err error, w http.ResponseWriter) {
	logger.Error(err)
	payload, _ := json.Marshal(Error(utils.ServiceError, responseEmptyMap))
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(payload))
}

func WriteServiceErrorWithDesc(logDescription string, err error, w http.ResponseWriter) {
	logger.Warn(logDescription, err)
	payload, _ := json.Marshal(Error(utils.ServiceError, responseEmptyMap))
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(payload))
}
