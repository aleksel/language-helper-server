package structure

import (
	"strings"
	"time"
)

const skyengTimeFormat = "2006-01-02 15:04:05"

type Request struct {
	Word string `json:"word" valid:"required~Required"`
}

type SuggestionRequest struct {
	SearchText string `json:"searchText" valid:"required~Required"`
}

type UserSearchSuggestionsResponse struct {
	Words []string `json:"words"`
}

type SkyEngResponse struct {
	Id       int       `json:"id"`
	Text     string    `json:"text"`
	Meanings []Meaning `json:"meanings"`
}

type Meaning struct {
	Id               int         `json:"id"`
	PartOfSpeechCode string      `json:"partOfSpeechCode"`
	Text             string      `json:"text"`
	Translation      Translation `json:"translation"`
	PreviewUrl       string      `json:"previewUrl"`
	ImageUrl         string      `json:"imageUrl"`
	Transcription    string      `json:"transcription"`
	SoundUrl         string      `json:"soundUrl"`
}

type Translation struct {
	Text string `json:"text"`
	Note string `json:"note"`
}

type MeaningRequest struct {
	Ids []int `json:"ids" valid:"required~Required"`
}

type SkyEngMeaningResponse struct {
	Id                             string                          `json:"id"`
	WordId                         int                             `json:"wordId"`
	DifficultyLevel                int                             `json:"difficultyLevel"`
	PartOfSpeechCode               string                          `json:"partOfSpeechCode"`
	Prefix                         string                          `json:"prefix"`
	Text                           string                          `json:"text"`
	SoundUrl                       string                          `json:"soundUrl"`
	Transcription                  string                          `json:"transcription"`
	Properties                     Properties                      `json:"properties"`
	UpdatedAt                      SkyengTime                      `json:"updatedAt"`
	Mnemonics                      string                          `json:"mnemonics"`
	Translation                    Translation                     `json:"translation"`
	Images                         []Image                         `json:"images"`
	Definition                     Definition                      `json:"definition"`
	Example                        []Example                       `json:"examples"`
	MeaningsWithSimilarTranslation []MeaningWithSimilarTranslation `json:"meaningsWithSimilarTranslation"`
	AlternativeTranslations        []AlternativeTranslation        `json:"alternativeTranslations"`
}

type SkyengTime struct {
	time.Time
}

func (t SkyengTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + t.Time.Format(skyengTimeFormat) + `"`), nil
}

func (t *SkyengTime) UnmarshalJSON(buf []byte) error {
	tt, err := time.Parse(skyengTimeFormat, strings.Trim(string(buf), `"`))
	if err != nil {
		return err
	}
	t.Time = tt
	return nil
}

type Properties struct {
	Countability      string `json:"countability"`
	IrregularPlural   bool   `json:"irregularPlural"`
	Irregular         bool   `json:"irregular"`
	PastTense         string `json:"pastTense"`
	PastParticiple    string `json:"pastParticiple"`
	PresentParticiple string `json:"presentParticiple"`
	Transitivity      string `json:"transitivity"`
	Collocation       bool   `json:"collocation"`
	PhrasalVerb       bool   `json:"phrasalVerb"`
	LinkingVerb       bool   `json:"linkingVerb"`
	SoundUrl          string `json:"soundUrl"`
}

type Image struct {
	Url string `json:"url"`
}

type Definition struct {
	Text     string `json:"text"`
	SoundUrl string `json:"soundUrl"`
}

type Example struct {
	Text     string `json:"text"`
	SoundUrl string `json:"soundUrl"`
}

type MeaningWithSimilarTranslation struct {
	MeaningId                int         `json:"meaningId"`
	FrequencyPercent         string      `json:"frequencyPercent"`
	PartOfSpeechAbbreviation string      `json:"partOfSpeechAbbreviation"`
	Translation              Translation `json:"translation"`
}

type AlternativeTranslation struct {
	Text        string      `json:"text"`
	Translation Translation `json:"translation"`
}
