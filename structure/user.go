package structure

import "time"

type User struct {
	TableName string    `sql:"language.users" json:"-"`
	Id        int       `json:"id"`
	Image     string    `json:"image"`
	Token     string    `json:"token" sql:"-"`
	FirstName string    `json:"firstName"`
	LastName  string    `json:"lastName"`
	Email     string    `json:"email"    valid:"required~Required,email"`
	Password  string    `json:"password,omitempty" valid:"required~Required"`
	Phone     string    `json:"phone"`
	UpdatedAt time.Time `json:"updatedAt" sql:",null"`
	CreatedAt time.Time `json:"createdAt" sql:",null"`
}

func Register(email string, password string) User {
	return User{Email: email, Password: password}
}

type UserSearch struct {
	TableName  string    `sql:"language.user_searches" json:"-"`
	Id         int       `json:"-"`
	SearchText string    `json:"searchText"`
	UserId     int       `json:"-"`
	Count      int64     `json:"count"`
	UpdatedAt  time.Time `json:"updatedAt" sql:",null"`
	CreatedAt  time.Time `json:"createdAt" sql:",null"`
}
