package structure

import "time"

type Token struct {
	TableName string    `sql:"language.tokens" json:"-"`
	Id        int       `json:"id"`
	UserId    int       `json:"userId"`
	Token     string    `json:"token"`
	ExpiredAt time.Time `json:"expiredAt"`
	CreatedAt time.Time `json:"createdAt" sql:",null"`
}
