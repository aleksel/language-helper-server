package logger

import (
	"github.com/sirupsen/logrus"
	"strings"
	"fmt"
	"language-helper-server/utils"
)

func Info(args ...interface{}) {
	logrus.Infoln(args...)
}

func Infof(format string, args ...interface{}) {
	logrus.Infof(format, args...)
}

func Warn(args ...interface{}) {
	logrus.Warnln(args...)
}

func Warnf(format string, args ...interface{}) {
	logrus.Warnf(format, args...)
}

func Debug(args ...interface{}) {
	logrus.Debugln(args...)
}

func Debugf(format string, args ...interface{}) {
	logrus.Debugf(format, args...)
}

func Error(args ...interface{}) {
	logrus.Errorln(args...)
}

func Errorf(format string, args ...interface{}) {
	logrus.Errorf(format, args...)
}

func Fatal(args ...interface{}) {
	logrus.Fatalln(args...)
}

func Fatalf(format string, args ...interface{}) {
	logrus.Fatalf(format, args...)
}

type log2LogrusWriter struct {
	entry *logrus.Entry
}

func (w *log2LogrusWriter) Write(b []byte) (int, error) {
	s := strings.Replace(string(b), " [31mstack[0m=\"", "\n\n[31mstack[0m\n", -1)
	for _, m := range strings.Split(s, "\\n") {
		fmt.Println(m)
	}
	return len(b), nil
}

func init() {
	
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true, DisableTimestamp: false, FullTimestamp: true})
	logrus.SetOutput(&log2LogrusWriter{entry: logrus.StandardLogger().WithField("logger", "std")})
	logrus.AddHook(StandardHook())
	if utils.DEV {
		logrus.SetLevel(logrus.DebugLevel)
	} else {
		logrus.ParseLevel(strings.ToLower(utils.LOG_LEVEL))
	}
}
