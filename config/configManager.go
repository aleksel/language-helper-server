package config

import (
	"github.com/spf13/viper"
	"os"
	"path"
	"language-helper-server/logger"
	"language-helper-server/utils"
	"language-helper-server/structure"
	jww "github.com/spf13/jwalterweatherman"
)

var configInstance *structure.Config

func Get() *structure.Config {
	if configInstance == nil {
		errorMessage := "ConfigManager isn't init, call first the \"InitConfig\" method"
		logger.Fatalf(errorMessage)
		panic(errorMessage)
	}
	return configInstance
}

func init() {
	var configuration structure.Config
	ex, err := os.Executable()
	
	envConfigName := "config"
	configPath := path.Dir(ex)
	if utils.DEV {
		// _, filename, _, _ := runtime.Caller(0)
		configPath = "./config/"
		jww.SetLogThreshold(jww.LevelDebug)
		jww.SetStdoutThreshold(jww.LevelDebug)
	}
	if utils.EnvProfileName != "" {
		envConfigName = "config_" + utils.EnvProfileName
	}
	if utils.EnvConfigPath != "" {
		configPath = utils.EnvConfigPath
	}
	
	viper.SetConfigName(envConfigName)
	viper.AddConfigPath(configPath)
	
	if err := viper.ReadInConfig(); err != nil {
		logger.Fatalf("Error reading config file, %s", err)
		panic(err)
	}
	err = viper.Unmarshal(&configuration)
	if err != nil {
		logger.Fatalf("unable to decode into struct, %v", err)
		panic(err)
	}
	
	configInstance = &configuration
}
