-- +goose Up

-- ----------------------------
-- Table structure for translates
-- ----------------------------
CREATE TABLE "translates" (
  "id" serial4 NOT NULL PRIMARY KEY,
  "to_language_id" int4 NOT NULL,
  "dictionary_id" int4,
  "text" text NOT NULL,
  "created_at"  timestamp DEFAULT (now() at time zone 'utc') NOT NULL,
  "updated_at"  timestamp DEFAULT (now() at time zone 'utc') NOT NULL
);

-- ----------------------------
-- Indexes structure for table translates
-- ----------------------------
CREATE INDEX "IX_translate_toLanguageIdId_languages_id" ON "translates" USING hash ("to_language_id");
CREATE INDEX "IX_translate_dictionaryId_dictionaries_id" ON "translates" USING hash ("dictionary_id");

-- ----------------------------
-- Foreign Key structure for table "translates"
-- ----------------------------
ALTER TABLE "translates" ADD FOREIGN KEY ("to_language_id") REFERENCES "languages" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "translates" ADD FOREIGN KEY ("dictionary_id") REFERENCES "dictionaries" ("id")
  ON DELETE SET NULL ON UPDATE SET NULL;

-- ----------------------------
-- Triggers for table translates
-- ----------------------------
CREATE TRIGGER update_create_update_time BEFORE UPDATE ON translates
    FOR EACH ROW EXECUTE PROCEDURE update_created_modified_column_date();

-- +goose Down
DROP TABLE IF EXISTS translates;
