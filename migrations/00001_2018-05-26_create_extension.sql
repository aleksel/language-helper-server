-- +goose Up
CREATE EXTENSION pg_trgm SCHEMA language;

-- +goose Down
DROP EXTENSION pg_trgm SCHEMA language;
