-- +goose Up

CREATE TABLE IF NOT EXISTS users (
  id serial4 PRIMARY KEY NOT NULL,
  role_id    INTEGER,
  image      VARCHAR(255),
  first_name VARCHAR(255),
  last_name  VARCHAR(255),
  email      VARCHAR(255) UNIQUE NOT NULL,
  password   VARCHAR(255),
  phone      VARCHAR(255) UNIQUE,
  updated_at      timestamp DEFAULT (now() at time zone 'utc') NOT NULL,
  created_at      timestamp DEFAULT (now() at time zone 'utc') NOT NULL
);

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "users" ADD UNIQUE ("email");
ALTER TABLE "users" ADD UNIQUE ("phone");

-- ----------------------------
-- Triggers for table users
-- ----------------------------
CREATE TRIGGER update_create_update_time BEFORE UPDATE ON users
    FOR EACH ROW EXECUTE PROCEDURE update_created_modified_column_date();

-- +goose Down
DROP TABLE IF EXISTS users;
