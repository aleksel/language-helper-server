-- +goose Up

CREATE TABLE IF NOT EXISTS user_searches (
  "id"          serial8 PRIMARY KEY NOT NULL,
  "search_text" varchar(255) NOT NULL,
  "user_id"     int4 NOT NULL,
  "count"       int4 NOT NULL DEFAULT 1,
  "updated_at"  timestamp DEFAULT (now() at time zone 'utc') NOT NULL,
  "created_at"  timestamp DEFAULT (now() at time zone 'utc') NOT NULL
);

-- ----------------------------
-- Indexes structure for table user_searches"
-- ----------------------------
CREATE INDEX "IX_userId_user_id" ON "user_searches" USING hash ("user_id");
CREATE UNIQUE INDEX "IX_userId_searchText" ON "user_searches" USING btree ("user_id", search_text);

-- ----------------------------
-- Foreign Key structure for table "user_searches"
-- ----------------------------
ALTER TABLE "user_searches" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Triggers for table users
-- ----------------------------
CREATE TRIGGER update_create_update_time BEFORE UPDATE ON user_searches
    FOR EACH ROW EXECUTE PROCEDURE update_created_modified_column_date();

-- +goose Down
DROP TABLE IF EXISTS user_searches;
