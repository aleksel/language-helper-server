-- +goose Up

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO languages VALUES (1, 'en');
INSERT INTO languages VALUES (2, 'ru');

-- +goose Down
DELETE FROM languages WHERE id IN (1, 2);
