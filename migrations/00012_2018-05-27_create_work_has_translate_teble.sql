-- +goose Up

-- ----------------------------
-- Table structure for word_has_translate
-- ----------------------------
CREATE TABLE "word_has_translate" (
  "word_id" int8 NOT NULL,
  "translate_id" int4 NOT NULL,
  "part_of_speech_id" int4 NOT NULL,
  PRIMARY KEY ("word_id", "translate_id", "part_of_speech_id")
);

-- ----------------------------
-- Indexes structure for table word_has_translate
-- ----------------------------
CREATE UNIQUE INDEX "IX_wordHasTranslate_wordId_word_id" ON "word_has_translate"
  USING btree ("word_id", "translate_id", "part_of_speech_id");
CREATE INDEX "IX_wordHasPartOfSpeech_partOfSpeechId_partOfSpeech_id"
  ON "word_has_translate" USING hash ("part_of_speech_id");
CREATE INDEX "IX_wordHasTranslate_translates_translate_id"
  ON "word_has_translate" USING hash ("translate_id");

-- ----------------------------
-- Foreign Key structure for table "word_has_translate"
-- ----------------------------
ALTER TABLE "word_has_translate" ADD FOREIGN KEY ("part_of_speech_id")
  REFERENCES "parts_of_speech" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "word_has_translate" ADD FOREIGN KEY ("translate_id")
  REFERENCES "translates" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "word_has_translate" ADD FOREIGN KEY ("word_id") REFERENCES "words" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE;


-- +goose Down
DROP TABLE IF EXISTS word_has_translate;
