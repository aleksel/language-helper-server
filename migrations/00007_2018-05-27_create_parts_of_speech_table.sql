-- +goose Up

-- ----------------------------
-- Table structure for parts_of_speech
-- ----------------------------
CREATE TABLE "parts_of_speech" (
  "id" serial4 NOT NULL PRIMARY KEY,
  "name" varchar(255) NOT NULL,
  "language_id" int4 NOT NULL,
  "created_at"  timestamp DEFAULT (now() at time zone 'utc') NOT NULL
);

-- ----------------------------
-- Indexes structure for table parts_of_speech
-- ----------------------------
CREATE INDEX "IX_languages_langusgeId_id" ON "parts_of_speech" USING hash ("language_id");

-- ----------------------------
-- Foreign Key structure for table "parts_of_speech"
-- ----------------------------
ALTER TABLE "parts_of_speech" ADD FOREIGN KEY ("language_id") REFERENCES "languages" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Triggers for table parts_of_speech
-- ----------------------------
CREATE TRIGGER update_create_update_time BEFORE UPDATE ON parts_of_speech
    FOR EACH ROW EXECUTE PROCEDURE update_created_column_date();

-- +goose Down
DROP TABLE IF EXISTS parts_of_speech;
