-- +goose Up

-- ----------------------------
-- Table structure for dictionaries
-- ----------------------------
CREATE TABLE "dictionaries" (
  "id" serial4 NOT NULL PRIMARY KEY,
  "name" varchar(255) NOT NULL,
  "discription" text,
  "created_at"  timestamp DEFAULT (now() at time zone 'utc') NOT NULL
);

-- ----------------------------
-- Triggers for table dictionaries
-- ----------------------------
CREATE TRIGGER update_create_update_time BEFORE UPDATE ON dictionaries
    FOR EACH ROW EXECUTE PROCEDURE update_created_column_date();

-- +goose Down
DROP TABLE IF EXISTS dictionaries;
