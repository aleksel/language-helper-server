-- +goose Up

-- ----------------------------
-- Table structure for languages
-- ----------------------------
CREATE TABLE languages (
  "id" serial4 NOT NULL PRIMARY KEY,
  "language" varchar(255) NOT NULL
);

-- +goose Down
DROP TABLE IF EXISTS languages;
