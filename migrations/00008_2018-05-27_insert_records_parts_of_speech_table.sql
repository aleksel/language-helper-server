-- +goose Up

-- ----------------------------
-- Records of parts_of_speech
-- ----------------------------
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('noun', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('pronoun', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('article', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('verb', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('adjective', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('adverb', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('number', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('proposition', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('conjunction', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('interjection', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('phrasal verbs', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('modal verb', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('determiner', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('unknown', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('auxiliary verb', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('combining form', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('contraction', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('exclamation', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('interrogative adverb', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('interrogative pronoun', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('particle', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('phrase', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('prefix', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('suffix', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('abbreviation', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('linking words', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('vague language', '1');
INSERT INTO "parts_of_speech" (name, language_id) VALUES ('fixed phrases', '1');

-- +goose Down
DELETE FROM parts_of_speech;
