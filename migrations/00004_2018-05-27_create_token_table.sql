-- +goose Up

CREATE TABLE "tokens" (
  "id" serial8 NOT NULL PRIMARY KEY,
  "user_id" int8 NOT NULL UNIQUE,
  "token" text NOT NULL,
  "expired_at" timestamp DEFAULT (now() at time zone 'utc') + '30 days'::interval NOT NULL,
  "created_at" timestamp DEFAULT (now() at time zone 'utc') NOT NULL
);

-- ----------------------------
-- Indexes structure for table tokens
-- ----------------------------
CREATE INDEX "IX_tokens_token" ON "tokens" USING hash ("token");

-- ----------------------------
-- Uniques structure for table tokens
-- ----------------------------
ALTER TABLE "tokens" ADD UNIQUE ("user_id");

-- ----------------------------
-- Foreign Key structure for table "tokens"
-- ----------------------------
ALTER TABLE "tokens" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id")
  ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Triggers for table tokens
-- ----------------------------
CREATE TRIGGER update_create_update_time BEFORE UPDATE ON tokens
    FOR EACH ROW EXECUTE PROCEDURE update_created_column_date();

-- +goose Down
DROP TABLE IF EXISTS tokens;
