-- +goose Up

-- ----------------------------
-- Table structure for words
-- ----------------------------
CREATE TABLE "words" (
  "id" serial4 NOT NULL PRIMARY KEY,
  "word" varchar(512) NOT NULL,
  "infinitive" varchar(255),
  "created_at"  timestamp DEFAULT (now() at time zone 'utc') NOT NULL
);

-- +goose StatementBegin
CREATE OR REPLACE FUNCTION make_tsvector(word VARCHAR(512))
RETURNS tsvector AS $$
  BEGIN
    RETURN to_tsvector(word);
  END
$$ LANGUAGE 'plpgsql' IMMUTABLE;
-- +goose StatementEnd

-- ----------------------------
-- Indexes structure for table words
-- ----------------------------
CREATE UNIQUE INDEX "IX_words_word" ON "words" USING btree ("word");
CREATE INDEX IF NOT EXISTS IX_words_word_gin ON "words" USING gin(make_tsvector(word));

-- ----------------------------
-- Triggers for table words
-- ----------------------------
CREATE TRIGGER update_create_update_time BEFORE UPDATE ON words
    FOR EACH ROW EXECUTE PROCEDURE update_created_column_date();

-- +goose Down
DROP TABLE IF EXISTS words;
