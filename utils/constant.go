package utils

import (
	"os"
	"strings"
)

// ===== ENV =====
var EnvProfileName = os.Getenv("APP_PROFILE")
var EnvConfigPath = os.Getenv("APP_CONFIG_PATH")
var DEV = strings.ToLower(os.Getenv("APP_MODE")) == "dev"
var LOG_LEVEL = os.Getenv("LOG_LEVEL")

// ===== ERRORS =====
const ServiceError = "Service is not available now, please try later"
const ValidationError = "Validation errors"

// ===== DATE =====
const FullDateFormat = "2006-01-02T15:04:05.999-07:00"
