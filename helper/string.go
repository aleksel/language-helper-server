package helper

import (
	"fmt"
	"strings"
)

func ArrayIntToString(arrayInt []int) string {
	return strings.Trim(strings.Join(strings.Fields(fmt.Sprint(arrayInt)), ","), "[]")
}
