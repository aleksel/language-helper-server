package helper

import (
	"encoding/json"
	"net/http"
	
	st "language-helper-server/structure"
	"github.com/asaskevich/govalidator"
	"language-helper-server/utils"
)

func ValidateAndWriteResult(request interface{}, w http.ResponseWriter) error {
	_, err := govalidator.ValidateStruct(request)
	if err != nil {
		validationErrors := govalidator.ErrorsByField(err)
		w.WriteHeader(http.StatusBadRequest)
		payload, _ := json.Marshal(st.ResponseMap{
			ErrorMessage: utils.ValidationError,
			Details:      validationErrors})
		
		w.Write([]byte(payload))
		return err
	}
	
	return nil
}
