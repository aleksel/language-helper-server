package main

import (
	"bufio"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
	"unicode/utf8"
	
	_ "github.com/lib/pq"
)

const (
	dbUser     = "language_helper"
	dbPassword = "X)3H^E.uW3u)MYw)"
	dbName     = "language_helper"
	dbHost     = "192.168.1.50"
)

type dict struct {
	dictionaryPath      string
	partOfSpeechPattern string
	toLanguageId        int
	dictionaryId        int
	partOfSpeech        int
}

var dictionaries = []dict{
	{
		dictionaryPath:      "d:\\data\\En-En-Oxford_Collocations.dsl",
		partOfSpeechPattern: `\[m1\]\[i\]\[c\]\s*(.+)\[\/c\]\[\/i\]\[\/m\]`,
		toLanguageId:        1,
		dictionaryId:        1,
	},
	{
		dictionaryPath:      "d:\\data\\En-En-Longman_DOCE5_Extras.dsl",
		partOfSpeechPattern: `\[m1\]\[i\]\[c\]\s*(.+)\[\/c\]\[\/i\]\[\/m\]`,
		toLanguageId:        1,
		dictionaryId:        2,
	},
	{
		dictionaryPath:      "d:\\data\\En-En-Longman_Phrasal_Verbs.dsl",
		partOfSpeechPattern: "",
		toLanguageId:        1,
		dictionaryId:        5,
		partOfSpeech:        11,
	},
	{
		dictionaryPath:      "d:\\data\\en-en_phrasal_verbs_as_1_0.dsl",
		partOfSpeechPattern: "",
		toLanguageId:        1,
		dictionaryId:        6,
		partOfSpeech:        11,
	},
	{
		dictionaryPath:      "d:\\data\\eng-eng_oxford_1_0.dsl",
		partOfSpeechPattern: `\[m1\]\[c green\]\s*(.+)\[\/c\]\[\/m\]`,
		toLanguageId:        1,
		dictionaryId:        4,
	},
	//chemotherapeutic
	{
		dictionaryPath:      "d:\\data\\eng-eng_UrbanDictionary2015_vmb_1_0_utf8.dsl",
		partOfSpeechPattern: "",
		toLanguageId:        1,
		dictionaryId:        3,
	},
}

const unknownPartOfSpeech = "unknown"

var partsOfSpeechTranslate = map[string]int{
	"phr verb":              11,
	"adj.":                  5,
	"adjective":             5,
	"adv.":                  6,
	"adverb":                6,
	"noun":                  1,
	"verb":                  4,
	"modalverb":             12,
	"modal verb":            12,
	"pronoun":               2,
	"conjunction":           9,
	"article":               3,
	"interjection":          10,
	"preposition":           8,
	"number":                7,
	"determiner":            13,
	unknownPartOfSpeech:     14,
	"auxiliary verb":        15,
	"cardinal number":       7,
	"combining form":        16,
	"contraction":           17,
	"exclamation":           18,
	"interrogative adverb":  20,
	"interrogative pronoun": 21,
	"ordinal number":        7,
	"particle":              22,
	"phrase":                23,
	"plural noun":           1,
	"possessive determiner": 13,
	"predeterminer":         13,
	"possessive pronoun":    2,
	"prefix":                24,
	"suffix":                25,
	"proper noun":           1,
	"relative adverb":       6,
	"relative pronoun":      2,
}

func parse() {
	filePaths := []string{
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\Lemmatized\\2+2+3lem.txt",
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\Lemmatized\\2+2+3frq.txt",
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\Lemmatized\\2+2+3cmn.txt",
		
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\American\\2of12.txt",
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\American\\2of12inf.txt",
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\American\\3esl.txt",
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\American\\6of12.txt",
		
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\International\\2of4brif.txt",
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\International\\3of6all.txt",
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\International\\3of6game.txt",
		"z:\\hitachi\\Dictionaries\\morphology\\12dict\\International\\5d+2a.txt",
	}
	
	reg, err := regexp.Compile(`[^a-zA-Z0-9-.,_& ]+`)
	check(err)
	
	words := make(Definitions)
	
	for _, value := range filePaths {
		scanner := readOpenFile(value)
		words = fillMap(words, scanner, reg)
	}
	
	fmt.Println(len(words))
	
	db, err := sql.Open("postgres",
		fmt.Sprintf("user=%s password=%s dbname=%s host=%s sslmode=disable",
			dbUser, dbPassword, dbName, dbHost))
	if err != nil {
		log.Fatal(err)
	}
	
	//rankingsJson, _ := json.Marshal(words)
	//err = ioutil.WriteFile("z:\\hitachi\\Dictionaries\\morphology\\12dict\\output.json", rankingsJson, 0644)
	
	for _, dict := range dictionaries {
		
		var pattern *regexp.Regexp
		if dict.partOfSpeechPattern != "" {
			pattern, err = regexp.Compile(dict.partOfSpeechPattern)
			check(err)
		}
		var wordsNew Definitions
		DeepCopy(&words, &wordsNew)
		wordsNew = readWordTranslation(dict.dictionaryPath, readOpenFile(dict.dictionaryPath),
			wordsNew, pattern, reg, dict.partOfSpeech)
		
		err = insert(db, wordsNew, dict, "SAVE INTO DB: "+dict.dictionaryPath)
		if err != nil {
			log.Fatal(err)
		}
	}
	
	fmt.Println()
	fmt.Println(len(words))
	defer db.Close()
}

func DeepCopy(a, b interface{}) {
	byt, _ := json.Marshal(a)
	json.Unmarshal(byt, b)
}

type Definition struct {
	Words       []string
	Parent      []string
	Translation map[int][]string
}

type Definitions map[string]*Definition

func readWordTranslation(name string,
	scanner *bufio.Scanner,
	words Definitions,
	patternPartOfSpeech *regexp.Regexp, reg *regexp.Regexp, partOfSpeechConfig int) Definitions {
	
	unknownPartOfSpeechNumber := partsOfSpeechTranslate[unknownPartOfSpeech]
	counter := 0
	activeCounter := 0
	loopTime := time.Now().Unix()
	allTime := time.Now().Unix()
	
	word := ""
	currentPartOfSpeech := 0
	definition := make(map[int][]string, 0)
	for scanner.Scan() {
		counter++
		byteLine := scanner.Bytes()
		for key, b := range byteLine {
			if b == 0 && len(byteLine) >= (key+1) {
				byteLine = append(byteLine[:key], byteLine[key+1:]...)
			}
		}
		line := removeNotUtfSymbols(string(byteLine))
		logProgress(name, counter, activeCounter, &loopTime, allTime, false)
		if len(line) > 2 && line[0:1] != "\t" && line[1:2] != "\t" {
			if _, ok := words[word]; !ok && word != "" {
				words[word] = &Definition{Translation: definition}
			} else if ok && word != "" && len(definition) == 0 {
				newWord := reg.ReplaceAllString(strings.ToLower(strings.TrimSpace(line)), "")
				if newWord != word {
					words[word].Words = AppendIfMissing(words[word].Words, newWord)
				}
				continue
			} else if ok && word != "" {
				if words[word].Translation != nil {
					for key, definitionMap := range definition {
						if _, ok := words[word].Translation[key]; ok {
							for _, value := range definitionMap {
								words[word].Translation[key] = append(words[word].Translation[key], value)
							}
						} else {
							words[word].Translation[key] = definitionMap
						}
					}
				} else {
					words[word].Translation = definition
				}
			}
			word = reg.ReplaceAllString(strings.ToLower(strings.TrimSpace(line)), "")
			definition = make(map[int][]string, 0)
			activeCounter++
		} else if len(reg.ReplaceAllString(strings.TrimSpace(line), "")) > 0 {
			line = line[1:]
			var partOfSpeech []string
			if patternPartOfSpeech != nil {
				partOfSpeech = patternPartOfSpeech.FindStringSubmatch(line)
				if len(partOfSpeech) == 2 {
					if value, ok := partsOfSpeechTranslate[partOfSpeech[1]]; ok {
						currentPartOfSpeech = value
						definition[value] = make([]string, 0)
						continue
					}
				}
				if currentPartOfSpeech != 0 {
					definition[currentPartOfSpeech] = append(definition[currentPartOfSpeech], line)
				} else if value, ok := definition[unknownPartOfSpeechNumber]; ok {
					definition[unknownPartOfSpeechNumber] = append(value, line)
				} else {
					definition[unknownPartOfSpeechNumber] = []string{line}
				}
			} else if partOfSpeechConfig != 0 {
				definition[partOfSpeechConfig] = append(definition[partOfSpeechConfig], line)
			} else {
				definition[unknownPartOfSpeechNumber] = append(definition[unknownPartOfSpeechNumber], line)
			}
		}
	}
	logProgress(name, counter, activeCounter, &loopTime, allTime, true)
	return words
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func IsLetter(s string) bool {
	for _, r := range s {
		if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') {
			return false
		}
	}
	return true
}

func logProgress(name string, counter int, activeCounter int, loopTime *int64, allTime int64, last bool) {
	if counter%1000 == 0 || last {
		fmt.Print("\r")
		currentTime := time.Now().Unix()
		fmt.Printf("Name: %10s, count: %10d, find: %10d, time spend: %10d, all time: %10d",
			name, counter, activeCounter, currentTime - *loopTime, currentTime-allTime)
		
		*loopTime = currentTime
	}
	if last {
		fmt.Println()
	}
}

func readOpenFile(filePath string) *bufio.Scanner {
	file, err := os.Open(filePath)
	check(err)
	//defer file.Close()
	
	scanner := bufio.NewScanner(file)
	buf := make([]byte, 0, 64*1024)
	scanner.Buffer(buf, 1024*1024)
	return scanner
}

func fillMap(wordsMap Definitions, scanner *bufio.Scanner, reg *regexp.Regexp) Definitions {
	
	var previousValue string
	var currentValue string
	var tempValue string
	
	for scanner.Scan() {
		tempValue = scanner.Text()
		currentValue = reg.ReplaceAllString(strings.TrimSpace(tempValue), "")
		if strings.HasPrefix(tempValue, "    ") {
			split := strings.Split(strings.TrimSpace(currentValue), ", ")
			for _, elem := range split {
				for _, elem2 := range strings.Split(strings.TrimSpace(elem), " - ") {
					elem2 = reg.ReplaceAllString(strings.TrimSpace(elem2), "")
					if _, ok := wordsMap[elem2]; !ok {
						wordsMap[elem2] = &Definition{Parent: []string{strings.TrimSpace(previousValue)}}
					} else {
						wordsMap[elem2].Parent = AppendIfMissing(wordsMap[elem2].Parent, previousValue)
					}
				}
			}
		} else if _, ok := wordsMap[currentValue]; !ok {
			wordsMap[currentValue] = &Definition{Parent: make([]string, 0)}
		}
		previousValue = currentValue
	}
	return wordsMap
}

func AppendIfMissing(slice []string, i string) []string {
	for _, ele := range slice {
		if ele == i {
			return slice
		}
	}
	return append(slice, i)
}

func insert(db *sql.DB, wordsMap Definitions, dict dict, name string) error {
	counter := 0
	activeCounter := 0
	newTranslateCounter := 0
	loopTime := time.Now().Unix()
	allTime := time.Now().Unix()
	tx, err := db.Begin()
	check(err)
	checkStmt, err := tx.Prepare("SELECT ws.id FROM language.words AS ws " +
	//"JOIN language.word_has_part_of_speech AS whps ON ws.id = whps.word_id " +
		"WHERE ws.word = $1")
	// AND whps.part_of_speech_id = $2
	check(err)
	
	stmt, err := tx.Prepare("INSERT INTO language.words (word, infinitive) VALUES ($1, $2) RETURNING id")
	check(err)
	
	stmt2, err := tx.Prepare("INSERT INTO language.translates (to_language_id, dictionary_id, text) " +
		"VALUES ($1, $2, $3) RETURNING id")
	check(err)
	
	stmt3, err := tx.Prepare("INSERT INTO language.word_has_translate VALUES ($1, $2, $3) ON CONFLICT (word_id, translate_id, part_of_speech_id) DO NOTHING;")
	check(err)
	
	for keyWord := range wordsMap {
		counter++
		logProgress(name, counter, activeCounter, &loopTime, allTime, false)
		if wordsMap[keyWord].Translation == nil || len(wordsMap[keyWord].Translation) == 0 {
			continue
		}
		for keyPartOfSpeech, value := range wordsMap[keyWord].Translation {
			activeCounter++
			var lastInsertedTranslateId int64 = 0
			err = stmt2.QueryRow(dict.toLanguageId, dict.dictionaryId,
				strings.Join(value, "\n")).Scan(&lastInsertedTranslateId)
			check(err)
			
			words := wordsMap[keyWord].Words
			words = AppendIfMissing(words, keyWord)
			
			for _, word := range words {
				row := checkStmt.QueryRow(word)
				var lastInsertedId int64 = 0
				err = row.Scan(&lastInsertedId)
				if err != nil || lastInsertedId == 0 {
					var parents []string
					if _, ok := wordsMap[word]; ok {
						parents = wordsMap[word].Parent
					}
					var parentWord string
					if len(parents) > 0 {
						parentWord = strings.Join(parents, ",")
					}
					err = stmt.QueryRow(word, parentWord).Scan(&lastInsertedId)
					check(err)
					
				}
				//_, err = stmt2.Exec(lastInsertedId, keyPartOfSpeech)
				//check(err)
				
				_, err = stmt3.Exec(lastInsertedId, lastInsertedTranslateId, keyPartOfSpeech)
				check(err)
				newTranslateCounter++
			}
		}
	}
	tx.Commit()
	logProgress(name, counter, activeCounter, &loopTime, allTime, true)
	fmt.Printf("NEW TRANSLATE: %d", newTranslateCounter)
	return nil
}

func removeNotUtfSymbols(text string) string {
	if !utf8.ValidString(text) {
		v := make([]rune, 0, len(text))
		for i, r := range text {
			if r == utf8.RuneError {
				_, size := utf8.DecodeRuneInString(text[i:])
				if size == 1 {
					continue
				}
			} else if r == 0 {
				continue
			}
			v = append(v, r)
		}
		return string(v)
	}
	return text
}
