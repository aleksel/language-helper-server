<?php

//302344
//20882
//$handle = fopen("d:\\data\\En-En-Oxford_Collocations.dsl", "r");
//206755
//3668
//$handle = fopen("d:\\data\\En-En_Macmillan English Thesaurus.dsl", "r");
//418954
//60684
//$handle = fopen("d:\\data\\En-En_Macmillan English Dictionary.dsl", "r");
//1526211
//32363
//$handle = fopen("d:\\data\\En-En-Longman_DOCE5_Extras.dsl", "r");
//1918273
//58171
//$handle = fopen("z:\\hitachi\\download\\eng-eng_oxford_1_0.dsl", "r");
//$handle = fopen("z:\\hitachi\\download\\eng-eng_UrbanDictionary2015_vmb_1_0_utf8.dsl", "r");

$sources = [
    'd:\\data\\En-En-Oxford_Collocations.dsl',
    'd:\\data\\En-En_Macmillan English Thesaurus.dsl',
    'd:\\data\\En-En_Macmillan English Dictionary.dsl',
    'd:\\data\\En-En-Longman_DOCE5_Extras.dsl',
    'z:\\hitachi\\download\\eng-eng_oxford_1_0.dsl',
];
$found  = 0;
$count = 0;
$startTime = time();
$allTime = time();
$words = [];

foreach ($sources as $source) {
    $handle = fopen($source, "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            ++$count;
            if (isset($line[0]) && $line[0] != "\t" && isset($line[1]) && $line[1] != "\t") {
                $line = trim($line);
                ++$found;
                if (!isset($words[$line])){
                    //echo $line . PHP_EOL;
                    $words[$line] = true;
                }
            }

            if ($count % 1000 === 0) {
                echo "\r";
                printf(
                    'count: %10d, find: %10d, time spend: %10d, all time: %10d',
                    $count,
                    $found,
                    time() - $startTime,
                    time() - $allTime
                );
                $allTime = time();
            }

        }

        echo $found . PHP_EOL;
        echo $count . PHP_EOL;
        fclose($handle);
    }

}
echo 'ALL: ' . count($words) . PHP_EOL;
