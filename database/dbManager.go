package database

import (
	"time"
	"github.com/go-pg/pg"
	_ "github.com/lib/pq"
	"database/sql"
	"github.com/pressly/goose"
	"reflect"
	"os"
	"language-helper-server/logger"
	"language-helper-server/utils"
	"language-helper-server/config"
	"language-helper-server/structure"
)

var (
	dbManagerInstance = DBManager{isInitialized: false}
	ormDbTypeName     = "orm.DB"
)

type DBManager struct {
	Db            *pg.DB
	isInitialized bool
}

func GetDBManager() *DBManager {
	return &dbManagerInstance
}

func RunInTransaction(f interface{}) error {
	db := GetDBManager().Db
	val := reflect.ValueOf(f)
	if val.Kind() == reflect.Func {
		t := val.Type()
		inParamCount := t.NumIn()
		params := make([]reflect.Value, inParamCount)
		tx, err := db.Begin()
		if err != nil {
			return err
		}
		for i := 0; i < inParamCount; i++ {
			param := t.In(i)
			if param.Kind() != reflect.Struct {
				err := "Invalid param type in callback. Expected struct with DB orm.DB field"
				logger.Error(err)
				panic(err)
			}
			field, present := param.FieldByName("DB")
			if !present || field.Type.String() != ormDbTypeName {
				err := "Invalid param type in callback. Expected struct with DB orm.DB field"
				logger.Error(err)
				panic(err)
			}
			repository := reflect.New(param)
			repository.Elem().FieldByName("DB").Set(reflect.ValueOf(tx))
			params[i] = repository.Elem()
		}
		var res []reflect.Value
		err = tx.RunInTransaction(func(tx *pg.Tx) error {
			res = val.Call(params)
			if len(res) == 0 {
				return nil
			} else {
				e := res[0].Interface()
				if err, ok := e.(error); ok {
					return err
				} else {
					return nil
				}
			}
		})
		return err
	} else {
		err := "Expected a function"
		logger.Error(err)
		panic(err)
	}
}

func createDbConnection(config structure.DBConfiguration) {
	if dbManagerInstance.Db != nil {
		dbManagerInstance.Db.Close()
	}
	var pdb = pg.Connect(&pg.Options{
		User:               config.Username,
		Password:           config.Password,
		Database:           config.Database,
		Addr:               config.Address + ":" + config.Port,
		MaxRetries:         5,
		IdleTimeout:        time.Duration(15) * time.Minute,
		IdleCheckFrequency: time.Duration(30) * time.Second,
	})
	pdb.WithParam("search_path", config.Schema)
	
	if utils.DEV {
		pdb.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
			query, err := event.FormattedQuery()
			if err != nil {
				panic(err)
			}
			
			logger.Debugf("%s %s", time.Since(event.StartTime), query)
		})
	}
	
	var n time.Time
	if _, err := pdb.QueryOne(pg.Scan(&n), "SELECT now()"); err != nil {
		panic(err)
	}
	
	logger.Infof("Database server time: %d", n)
	dbManagerInstance = DBManager{Db: pdb, isInitialized: true}
}

func init() {
	config := config.Get()
	configDb := config.Db
	if configDb.CreateSchema {
		db, err := openSqlConn(configDb, "public")
		defer db.Close()
		
		if err != nil {
			logger.Fatal(err)
			panic(err)
		}
		_, e := db.Exec("CREATE SCHEMA IF NOT EXISTS " + configDb.Schema)
		if e != nil {
			logger.Fatalf("Error creating schema: %s", e)
			panic(e)
		}
	}
	
	db, err := openSqlConn(configDb, configDb.Schema)
	defer db.Close()
	
	if err != nil {
		logger.Fatal(err)
		panic(err)
	}
	
	migrationDir := "migrations"
	if _, err := os.Stat(migrationDir); !os.IsNotExist(err) {
		if err := goose.Run("up", db, migrationDir); err != nil {
			logger.Fatalf("goose run: %v", err)
			panic(err)
		}
	}
	
	createDbConnection(configDb)
}

func openSqlConn(config structure.DBConfiguration, schema string) (*sql.DB, error) {
	return sql.Open("postgres", "postgres://"+
		config.Address+ ":"+ config.Port+
		"/"+ config.Database+
		"?search_path="+ schema+ "&sslmode=disable&user="+
		config.Username+ "&password="+ config.Password)
}
